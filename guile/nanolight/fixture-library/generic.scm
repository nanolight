(define-module (nanolight fixture-library generic)
  #:use-module (oop goops)
  #:use-module (nanolight fixture)
  #:export (generic-dimmer generic-rgb))

(use-modules (srfi srfi-1))


(define (generic-dimmer)
  (list
    (make <fixture-attribute> #:name 'intensity
      #:range '(0 100) #:type 'continuous #:home-value 0
      #:translator (lambda (universe start-addr value set-dmx)
                     (set-dmx universe start-addr
                       (percent->dmxval value))))))


(define (feature-char->attr-name c)
  (case c
    ((i) 'intensity)
    ((r) 'red)
    ((g) 'green)
    ((b) 'blue)
    ((w) 'white)
    (else
     (error "Unrecognised symbol for generic RGB fixture" c))))


(define (generic-rgb feature-list)
  (lambda ()
    (fold
     (lambda (feature addr-offset list-so-far)
       (if (eq? feature 0)
           list-so-far
           (cons
            (make <fixture-attribute>
              #:name (feature-char->attr-name feature)
              #:range '(0 100)
              #:type 'continuous
              #:home-value 0
              #:translator (lambda (universe start-addr value set-dmx)
                             (set-dmx universe
                                      (+ start-addr addr-offset)
                                      (percent->dmxval value))))
            list-so-far)))

     '()
     feature-list
     (iota (length feature-list) 0))))
