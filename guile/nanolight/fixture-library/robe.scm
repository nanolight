(define-module (nanolight fixture-library robe)
  #:use-module (oop goops)
  #:use-module (nanolight fixture)
  #:export (robe-dl7s-profile-mode1))


(define (robe-dl7s-profile-mode1)
  (list

    (make <fixture-attribute> #:name 'pan
      #:range '(0 540) #:type 'continuous #:home-value 270
      #:translator (lambda (universe start-addr value set-dmx)
                     (let ((val16 (* value (/ 65536 540))))
                       (set-dmx universe (chan 1 start-addr)
                         (msb val16))
                       (set-dmx universe (chan 2 start-addr)
                         (lsb val16)))))

    (make <fixture-attribute> #:name 'tilt
      #:range '(0 270) #:type 'continuous #:home-value 135
      #:translator (lambda (universe start-addr value set-dmx)
                     (let ((val16 (* value (/ 65536 270))))
                       (set-dmx universe (chan 3 start-addr)
                         (msb val16))
                       (set-dmx universe (chan 4 start-addr)
                         (lsb val16)))))

    (make <fixture-attribute> #:name 'strobe
      #:range '(#f #t) #:type 'step #:home-value #f
      #:translator (lambda (universe start-addr value set-dmx)
                     (set-dmx universe (chan 49 start-addr)
                       (if value 95 32))))

    (make <fixture-attribute> #:name 'intensity
      #:range '(0 100) #:type 'continuous #:home-value 0
      #:translator (lambda (universe start-addr value set-dmx)
                     (let ((val16 (* value (/ 65536 100))))
                       (set-dmx universe (chan 50 start-addr)
                         (msb val16))
                       (set-dmx universe (chan 51 start-addr)
                         (lsb val16)))))

    (make <fixture-attribute> #:name 'prism
      #:range '(#f #t) #:type 'step #:home-value #f
      #:translator (lambda (universe start-addr value set-dmx)
                     (set-dmx universe (chan 28 start-addr)
                       (if value 50 0))))))
