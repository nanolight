/*
 * robe_dl7s.h
 *
 * Copyright © 2019-2020 Thomas White <taw@bitwiz.me.uk>
 *
 * This file is part of NanoLight.
 *
 * NanoLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

static int dl7s_magic_chans[] = {48};
static int dl7s_magic_vals[] = {32};
static int dl7s_gobo_chans[] = {24, 22};
static int dl7s_gobo_vals[] = { 0,   0,
                                6,   0,
                               10,   0,
                               15,   0,
                               19,   0,
                               24,   0,
                               28,   0,
                               33,   0,
                               38,   0,
                               42,   0,
                               47,   0,
                               52,   0,
                               57,   0,
                                0,  67,
                                0,  73,
                                0,  78,
                                0,  84,
                                0,  89,
                                0,  95,
                                0, 100,
                                0, 106 };
static int dl7s_gobo_flags[] = { 0,
                                 GOBO_ROTATE,
                                 GOBO_ROTATE,
                                 GOBO_ROTATE,
                                 GOBO_ROTATE,
                                 GOBO_ROTATE,
                                 GOBO_ROTATE,
                                 GOBO_SPIN,
                                 GOBO_SPIN,
                                 GOBO_SPIN,
                                 GOBO_SPIN,
                                 GOBO_SPIN,
                                 GOBO_SPIN,
                                 0,
                                 0,
                                 0,
                                 0,
                                 0,
                                 0,
                                 0,
                                 0 };
static int dl7s_prism_chans[] = {27};
static int dl7s_prism_vals[] = {0, 50};
static int dl7s_prism_flags [] = {0, PRISM_SPIN};

struct fixture_class dl7s_class = {

	.name = "Robe Robin DL7S Profile Mode 1",

	.properties = COL_CMY,
	.attributes = INTENSITY | COLOUR | PANTILT | FOCUS | ZOOM
	                  | FROST | IRIS | GOBO | PRISM,
	.attributes16 = INTENSITY | COLOUR | PANTILT | FOCUS | ZOOM
	                    | IRIS,

	.intensity_offset = 49,
	.pan_offset = 0,
	.tilt_offset = 2,
	.cyan_offset = 8,
	.magenta_offset = 10,
	.yellow_offset = 12,
	.focus_offset = 34,
	.zoom_offset = 32,
	.frost_offset = 29,  /* FIXME 0..179 only */
	.iris_offset = 30,  /* FIXME: 0..179 only, but also fine @ offset 32 */
	.gobo_rotate_offset = 25,
	.gobo_spin_offset = 25,
	.prism_rotate_offset = 0,
	.prism_spin_offset = 28,

	.n_magic = 1,
	.magic_chans = dl7s_magic_chans,
	.magic_vals = dl7s_magic_vals,

	.n_gobos = 21,
	.n_gobo_chans = 2,
	.gobo_chans = dl7s_gobo_chans,
	.gobo_vals = dl7s_gobo_vals,
	.gobo_flags = dl7s_gobo_flags,

	.n_prisms = 2,
	.n_prism_chans = 1,
	.prism_chans = dl7s_prism_chans,
	.prism_vals = dl7s_prism_vals,
	.prism_flags = dl7s_prism_flags,
};
