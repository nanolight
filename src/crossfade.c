/*
 * crossfade.c
 *
 * Copyright © 2019 Thomas White <taw@bitwiz.me.uk>
 *
 * This file is part of NanoLight.
 *
 * NanoLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <glib.h>

#include "lightctx.h"


static void fade(float start, float target, float frac, float *result)
{
	float val = start + (target - start)*frac;

	/* HTP merge */
	if ( val > *result ) *result = val;
}


void calculate_fades(struct lightctx *nl)
{
	int i;
	float now = g_get_monotonic_time()/1e6;  /* seconds */

	for ( i=0; i<nl->n_fades; i++ ) {

		int j;
		struct fade *f = &nl->fades[i];
		float st = f->start_time/1e6;
		float frac;

		if ( !f->active ) continue;

		frac = ((now-st)-f->delay)/f->fade;
		if ( frac > 1.0 ) {
			frac = 1.0;
			f->active = 0;
		}
		if ( frac < 0.0 ) frac = 0.0;

		for ( j=0; i<f->n_fixtures; i++ ) {
			struct fade_fix *fix = &f->fixtures[j];
			if ( fix->attrs & INTENSITY ) {
				fade(fix->start.intensity, fix->target.intensity, frac, &fix->fix->v.intensity);
			}
			if ( fix->attrs & COLOUR ) {
				fade(fix->start.cyan, fix->target.cyan, frac, &fix->fix->v.cyan);
				fade(fix->start.magenta, fix->target.magenta, frac, &fix->fix->v.magenta);
				fade(fix->start.yellow, fix->target.yellow, frac, &fix->fix->v.yellow);
				fade(fix->start.red, fix->target.red, frac, &fix->fix->v.red);
				fade(fix->start.green, fix->target.green, frac, &fix->fix->v.green);
				fade(fix->start.blue, fix->target.blue, frac, &fix->fix->v.blue);
			}
			if ( fix->attrs & PANTILT ) {
				fade(fix->start.pan, fix->target.pan, frac, &fix->fix->v.pan);
				fade(fix->start.tilt, fix->target.tilt, frac, &fix->fix->v.tilt);
			}
		}

	}
}


void test_fade(struct lightctx *nl)
{
	struct fade *f;

	nl->fades = malloc(sizeof(struct fade));
	f = &nl->fades[0];

	f->fixtures = malloc(64*sizeof(struct fade_fix));
	f->n_fixtures = 0;

	f->fixtures[0].fix = &nl->fixtures[0];
	f->fixtures[0].attrs = INTENSITY;
	f->fixtures[0].target.intensity = 1.0;
	f->n_fixtures = 1;
	f->fade = 1.0;
	f->delay = 0.0;

	/* Start the fade */
	f->fixtures[0].start = nl->fixtures[0].v;
	f->start_time = g_get_monotonic_time();
	f->active = 1;
	nl->n_fades = 1;
}

