/*
 * nanolight.h
 *
 * Copyright © 2019-2020 Thomas White <taw@bitwiz.me.uk>
 *
 * This file is part of NanoLight.
 *
 * NanoLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef LIGHTCTX_H
#define LIGHTCTX_H

#include <gtk/gtk.h>

#include "scheme.h"

/* Fixture attributes */
#define INTENSITY    (1<<0)
#define COLOUR       (1<<1)
#define PANTILT      (1<<2)
#define FOCUS        (1<<3)
#define ZOOM         (1<<4)
#define FROST        (1<<5)
#define IRIS         (1<<6)
#define SHUTTERS     (1<<7)
#define GOBO         (1<<8)
#define GOBO_ROTATE  (1<<9)
#define GOBO_SPIN    (1<<10)
#define PRISM        (1<<11)
#define PRISM_ROTATE (1<<12)
#define PRISM_SPIN   (1<<13)

/* Fixture class properties */
#define COL_RGB  (1<<0)
#define COL_CMY  (1<<1)

/* Fixture properties */
#define REVERSE_PAN  (1<<0)
#define REVERSE_TILT  (1<<1)

struct fixture_class
{
	char *name;
	int properties;
	int attributes;
	int attributes16;

	int n_magic;
	int *magic_chans;
	int *magic_vals;

	int intensity_offset;
	int pan_offset;
	int tilt_offset;

	/* For CMY colour */
	int cyan_offset;
	int magenta_offset;
	int yellow_offset;

	/* For RGB colour */
	int red_offset;
	int green_offset;
	int blue_offset;

	int focus_offset;
	int zoom_offset;
	int frost_offset;
	int iris_offset;

	int n_gobos;   /* Number of gobos including "no gobo" */
	int n_gobo_chans;
	int *gobo_chans;
	int *gobo_vals;
	int *gobo_flags;
	int gobo_rotate_offset;
	int gobo_spin_offset;

	int n_prisms;
	int n_prism_chans;
	int *prism_chans;
	int *prism_vals;
	int *prism_flags;
	int prism_rotate_offset;
	int prism_spin_offset;
};


struct attr_vals
{
	float intensity;      /* 0 (off) to 1 (full) */
	float cyan;           /* 0 (no filter) to 1 (full colour) */
	float magenta;        /* 0 (no filter) to 1 (full colour) */
	float yellow;         /* 0 (no filter) to 1 (full colour) */
	float red;            /* 0 (no red) to 1 (full on) */
	float green;          /* 0 (no green) to 1 (full on) */
	float blue;           /* 0 (no blue) to 1 (full on) */
	float pan;            /* -1 (fully stage left) to +1 (fully stage right) */
	float tilt;           /* -1 (fully downstage) to +1 (fully upstage) */
	int gobo;             /* Gobo number: 0 to cls->n_gobos-1 inclusive */
	float gobo_rotate;    /* -1 (fully anticlockwise) to +1 (fully clockwise) */
	float gobo_spin;      /* -1 (fastest anticlockwise) to +1 (fastest clockwise) */
	int prism;            /* Exactly like gobo */
	float prism_rotate;   /* Exactly like gobo_rotate */
	float prism_spin;     /* Exactly like gobo_spin */
	float focus;          /* 0 (nearest) to 1 (farthest) */
	float zoom;           /* 0 (narrowest) to 1 (widest) */
	float frost;          /* 0 (hardest) to 1 (softest) */
	float iris;           /* 0 (fully open) to 1 (fully closed) */
};


struct fixture
{
	char *label;
	int universe;
	int base_addr;
	struct fixture_class *cls;

	int flags;

	struct attr_vals v;

	/* Values at start of mouse movement */
	float pan_start;
	float tilt_start;
};


struct fade_fix
{
	struct fixture *fix;
	int attrs;             /* Bit mask of attributes to change */
	struct attr_vals start;
	struct attr_vals target;
};


struct fade
{
	struct fade_fix *fixtures;
	int n_fixtures;

	gint64 start_time;
	int active;

	float fade;
	float delay;
};


struct lightctx
{
	int n_fixtures;
	int max_fixtures;
	struct fixture *fixtures;

	struct fade *fades;
	int n_fades;

	GtkIMContext *im_context;
	GtkWidget *da;

	double fixture_width;

	char cmdline[1024];
	int cursor_idx;
	PangoLayout *layout;
	PangoLayout *sa_layout;

	int selection[1024];
	int n_sel;
	int sel_attr;
	int dragging;
	int fine;
	int go_lock;
	int sb_lock;

	double x_orig;
	double y_orig;
};

extern void attr_movex(struct lightctx *nl, signed int d, int fine);
extern void attr_movey(struct lightctx *nl, signed int d, int fine);

extern int any_selected_fixture_has(struct lightctx *nl, int attr);

extern struct fixture *create_fixture(struct lightctx *nl, struct fixture_class *cls,
                                      const char *label, int universe, int base_addr,
                                      int flags);

#endif	/* LIGHTCTX_H */
