/*
 * nanolight.c
 *
 * Copyright © 2019-2020 Thomas White <taw@bitwiz.me.uk>
 *
 * This file is part of NanoLight.
 *
 * NanoLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include <gtk/gtk.h>
#include <getopt.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <libguile.h>

#include <libintl.h>
#define _(x) gettext(x)

#include "lightctx.h"
#include "scanout.h"
#include "display.h"
#include "scheme.h"

static void show_help(const char *s)
{
	printf(_("Syntax: %s [options]\n\n"), s);
	printf(_("Theatrical lighting control program.\n\n"
	         "  -h, --help    Display this help message.\n"));
}


static gboolean scanout_cb(gpointer data)
{
	scanout_all((struct lightctx *)data);
	return G_SOURCE_CONTINUE;
}


int main(int argc, char *argv[])
{
	struct lightctx nl;
	int c;
	pthread_t repl_thread;

	gtk_init(&argc, &argv);

	const struct option longopts[] = {
		{"help",               0, NULL,               'h'},
		{0, 0, NULL, 0}
	};

	while ((c = getopt_long(argc, argv, "h", longopts, NULL)) != -1) {

		switch (c)
		{
			case 'h' :
			show_help(argv[0]);
			return 0;

			case 0 :
			break;

			default :
			return 1;
		}

	}

#if !GLIB_CHECK_VERSION(2,36,0)
	g_type_init();
#endif

	bindtextdomain("nanolight", LOCALEDIR);
	textdomain("nanolight");

	nl.fixture_width = 80.0;
	nl.fixtures = NULL;
	nl.n_fixtures = 0;
	nl.max_fixtures = 0;
	nl.cmdline[0] = '\0';
	nl.cursor_idx = 0;
	nl.n_sel = 0;
	nl.sel_attr = INTENSITY;
	nl.dragging = 0;
	nl.go_lock = 0;
	nl.sb_lock = 0;
	nl.n_fades = 0;

	/* Set up output */
	g_timeout_add(50, scanout_cb, &nl);

	pthread_create(&repl_thread, NULL, run_repl, &nl);

	create_main_window(&nl);
	gtk_main();

	return 0;
}
