/*
 * lightctx.c
 *
 * Copyright © 2019 Thomas White <taw@bitwiz.me.uk>
 *
 * This file is part of NanoLight.
 *
 * NanoLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "lightctx.h"


struct fixture *create_fixture(struct lightctx *nl, struct fixture_class *cls,
                               const char *label, int universe, int base_addr,
                               int flags)
{
	struct fixture *fix;

	if ( nl->n_fixtures == nl->max_fixtures ) {
		struct fixture *fixtures_new;
		fixtures_new = realloc(nl->fixtures, (64+nl->max_fixtures)*sizeof(struct fixture));
		if ( fixtures_new == NULL ) return NULL;
		nl->fixtures = fixtures_new;
		nl->max_fixtures += 64;
	}

	fix = &nl->fixtures[nl->n_fixtures++];
	fix->label = strdup(label);
	fix->universe = universe;
	fix->base_addr = base_addr;
	fix->cls = cls;
	fix->flags = flags;

	fix->v.intensity = 0.0;
	fix->v.cyan = 0.0;
	fix->v.magenta = 0.0;
	fix->v.yellow = 0.0;
	fix->v.red = 0.0;
	fix->v.green = 0.0;
	fix->v.blue = 0.0;
	fix->v.pan = 0.0;
	fix->v.tilt = 0.0;
	fix->v.gobo = 0;
	fix->v.gobo_rotate = 0.0;
	fix->v.gobo_spin = 0.0;
	fix->v.prism = 0;
	fix->v.prism_rotate = 0.0;
	fix->v.prism_spin = 0.0;
	fix->v.focus = 0.5;
	fix->v.zoom = 0.5;
	fix->v.frost = 0.0;

	return fix;
}


static void cap_value(float *val, float min, float max)
{
	if ( *val > max ) *val = max;
	if ( *val < min ) *val = min;
}


void attr_movex(struct lightctx *nl, signed int d, int fine)
{
	int i;
	float chg = fine ? d/60000.0 : d/10.0;
	if ( nl->sel_attr != PANTILT ) return;
	for ( i=0; i<nl->n_sel; i++ ) {
		struct fixture *fix = &nl->fixtures[nl->selection[i]];
		if ( !(fix->cls->attributes & PANTILT) ) continue;
		fix->v.pan += chg;
		cap_value(&fix->v.pan, -1.0, 1.0);
	}
}


void attr_movey(struct lightctx *nl, signed int d, int fine)
{
	int i;
	float chg = fine ? d/60000.0 : d/10.0;
	for ( i=0; i<nl->n_sel; i++ ) {
		struct fixture *fix = &nl->fixtures[nl->selection[i]];

		switch ( nl->sel_attr ) {

			case INTENSITY :
			fix->v.intensity += chg;
			cap_value(&fix->v.intensity, 0.0, 1.0);
			break;

			case PANTILT :
			fix->v.tilt += chg;
			cap_value(&fix->v.tilt, -1.0, 1.0);
			break;

			case FOCUS :
			fix->v.focus += chg;
			cap_value(&fix->v.focus, 0.0, 1.0);
			break;

			case ZOOM :
			fix->v.zoom += chg;
			cap_value(&fix->v.zoom, 0.0, 1.0);
			break;

			case FROST :
			fix->v.frost += chg;
			cap_value(&fix->v.frost, 0.0, 1.0);
			break;

			case IRIS :
			fix->v.iris += chg;
			cap_value(&fix->v.iris, 0.0, 1.0);
			break;

			case GOBO :
			if ( (fix->v.gobo == 0) && (d<0) ) continue;
			if ( (fix->v.gobo == fix->cls->n_gobos-1) && (d>0) ) continue;
			fix->v.gobo += d;
			break;

			case GOBO_ROTATE :
			fix->v.gobo_rotate += chg;
			cap_value(&fix->v.gobo_rotate, -1.0, 1.0);
			break;

			case GOBO_SPIN :
			fix->v.gobo_spin += chg;
			cap_value(&fix->v.gobo_spin, -1.0, 1.0);
			break;

			case PRISM :
			if ( (fix->v.prism == 0) && (d<0) ) continue;
			if ( (fix->v.prism == fix->cls->n_prisms-1) && (d>0) ) continue;
			fix->v.prism += d;
			break;

			case PRISM_ROTATE :
			fix->v.prism_rotate += chg;
			cap_value(&fix->v.prism_rotate, -1.0, 1.0);
			break;

			case PRISM_SPIN :
			fix->v.prism_spin += chg;
			cap_value(&fix->v.prism_spin, -1.0, 1.0);
			break;

		}

	}
}


int any_selected_fixture_has(struct lightctx *nl, int attr)
{
	int i;
	for ( i=0; i<nl->n_sel; i++ ) {
		struct fixture *fix = &nl->fixtures[nl->selection[i]];
		if ( fix->cls->attributes & attr ) return 1;
		if ( fix->cls->attributes & GOBO ) {
			if ( fix->cls->gobo_flags[fix->v.gobo] & attr ) return 1;
		}
		if ( fix->cls->attributes & PRISM ) {
			if ( fix->cls->prism_flags[fix->v.prism] & attr ) return 1;
		}
	}
	return 0;
}
